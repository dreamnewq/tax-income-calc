package org.acme.funqy;


import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import org.acme.funqy.configuration.TaxConfiguration;
import org.acme.funqy.controller.SalaryRequest;
import org.acme.funqy.controller.SalaryResponseDecimal;
import org.acme.funqy.db.TaxBand;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;


@ApplicationScoped
public class TaxCalculatorService {


    @ConfigProperty(name = "aws.secretmanager.dbcredentials-secret")
    String dbSecretName;


    @Inject
    TaxConfiguration taxConfiguration;

    private static final int COUNT_OF_MONTHS_IN_YEAR = 12;

    public SalaryResponseDecimal calculateTax(SalaryRequest request) {
        List<TaxBand> taxBandsSorted = taxConfiguration.getTaxBands();
        if (taxBandsSorted == null) {
            throw new IllegalStateException("Tax bands data is not initialized.");
        }

        BigDecimal grossAnnualSalary = BigDecimal.valueOf(request.grossAnnualSalary());
        BigDecimal annualTaxPaid = calculateAnnualTax(grossAnnualSalary, taxBandsSorted);

        return constructSalaryResponse(request, annualTaxPaid);
    }


    private SalaryResponseDecimal constructSalaryResponse(SalaryRequest request, BigDecimal annualTaxPaid) {
        BigDecimal grossAnnualSalary = BigDecimal.valueOf(request.grossAnnualSalary());
        BigDecimal grossMonthlySalary = grossAnnualSalary.divide(BigDecimal.valueOf(COUNT_OF_MONTHS_IN_YEAR), 2, RoundingMode.HALF_UP);

        // Directly calculate the net annual and monthly salaries
        BigDecimal netAnnualSalary = grossAnnualSalary.subtract(annualTaxPaid);
        BigDecimal netMonthlySalary = netAnnualSalary.divide(BigDecimal.valueOf(COUNT_OF_MONTHS_IN_YEAR), 2, RoundingMode.HALF_UP);

        BigDecimal monthlyTaxPaid = annualTaxPaid.divide(BigDecimal.valueOf(COUNT_OF_MONTHS_IN_YEAR), 2, RoundingMode.HALF_UP);

        return new SalaryResponseDecimal(
                round(grossAnnualSalary),
                round(grossMonthlySalary),
                round(netAnnualSalary),
                round(netMonthlySalary),
                round(annualTaxPaid),
                round(monthlyTaxPaid)
        );
    }

    private static BigDecimal calculateAnnualTax(BigDecimal grossAnnualSalary, List<TaxBand> taxBandsSorted) {
        BigDecimal tax = BigDecimal.ZERO;

        for (TaxBand band : taxBandsSorted) {
            if (grossAnnualSalary.compareTo(band.lowerLimit()) > 0) {
                BigDecimal taxableAmount = grossAnnualSalary.min(band.upperLimit()).subtract(band.lowerLimit());
                BigDecimal bandTax = taxableAmount.multiply(band.percent()).divide(BigDecimal.valueOf(100), RoundingMode.HALF_UP);
                tax = tax.add(bandTax);
            }
        }

        return tax;
    }

    private static BigDecimal round(BigDecimal number) {
        return number.setScale(2, RoundingMode.HALF_UP);
    }

}
