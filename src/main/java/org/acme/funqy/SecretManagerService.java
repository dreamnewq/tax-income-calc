package org.acme.funqy;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import software.amazon.awssdk.services.secretsmanager.SecretsManagerClient;
import software.amazon.awssdk.services.secretsmanager.model.GetSecretValueRequest;

import java.lang.reflect.Type;
import java.util.Map;

@ApplicationScoped
public class SecretManagerService {

    private static final String VERSION_STAGE = "AWSCURRENT";
    private static final Type JSONOBJECT_TO_MAP_TYPE = new TypeToken<Map<String, String>>(){}.getType();
    private static final Gson GSON = new Gson();

    @Inject
    SecretsManagerClient secretsManagerClient;


    public Map<String, String> parseAllKeyValuesFromSecret(String secretName) {
        String jsonStr = loadSecret(secretName);
        return GSON.fromJson(jsonStr, JSONOBJECT_TO_MAP_TYPE);
    }


    private String loadSecret(String secretName) {
        GetSecretValueRequest getSecretValueRequest = GetSecretValueRequest.builder()
                .secretId(secretName)
                .versionStage(VERSION_STAGE)
                .build();
        return secretsManagerClient
                .getSecretValue(getSecretValueRequest)
                .secretString();
    }
}
