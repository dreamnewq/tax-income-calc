package org.acme.funqy.controller;

import io.quarkus.funqy.Funq;
import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;
import org.acme.funqy.TaxCalculatorService;
import org.acme.funqy.init.ApplicationInitializer;
import org.eclipse.microprofile.metrics.annotation.Timed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TaxesController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TaxesController.class);

    @Inject
    TaxCalculatorService taxCalculatorService;

    @Inject
    ApplicationInitializer applicationInitializer;

    @PostConstruct
    public void ensureInitialization() {
        LOGGER.info("TaxesController.postconstruct start");
        applicationInitializer.init();
        LOGGER.info("TaxesController.postconstruct end");
    }

    @Funq
    @Timed(name = "calcTime", description = "Time taken by calc method")
    public SalaryResponseDecimal calc(SalaryRequest request) {
        LOGGER.info("Calculation called with request: {}", request);
        return taxCalculatorService.calculateTax(request);
    }
}
