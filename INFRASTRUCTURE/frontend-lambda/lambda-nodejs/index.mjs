import { promises as fs } from 'fs';
import { dirname, join } from 'path';
import { fileURLToPath } from 'url';

const __dirname = dirname(fileURLToPath(import.meta.url));

export async function handler(event) {
    // Asynchronously read the HTML file from the same directory as this script
    const html = await fs.readFile(join(__dirname, 'index.html'), 'utf8');

    return {
        statusCode: 200,
        headers: { "Content-Type": "text/html" },
        body: html
    };
}
