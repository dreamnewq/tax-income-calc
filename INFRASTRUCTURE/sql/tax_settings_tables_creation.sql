-- Helper queries:
--
-- use TaxConfigDb;
-- select * from taxation;
-- select * from tax_band;

CREATE TABLE IF NOT EXISTS taxation (
                                        id BIGINT AUTO_INCREMENT PRIMARY KEY,
                                        name VARCHAR(255) NOT NULL UNIQUE,
                                        description VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS tax_band (
                                        id BIGINT AUTO_INCREMENT PRIMARY KEY,
                                        taxation_id BIGINT NOT NULL,
                                        lower_limit DECIMAL(30, 2) NOT NULL,
                                        upper_limit DECIMAL(30, 2) NOT NULL,
                                        percent DECIMAL(5, 2) NOT NULL,
                                        FOREIGN KEY (taxation_id) REFERENCES taxation(id)
);
INSERT INTO taxation (name, description) VALUES ('UK_PROGRESSIVE', 'Step progressive taxation in UK in 2024');



INSERT INTO tax_band (taxation_id, lower_limit, upper_limit, percent) VALUES
      ((SELECT id FROM taxation WHERE name = 'UK_PROGRESSIVE'), 0.00,       5000.00, 0.00),
      ((SELECT id FROM taxation WHERE name = 'UK_PROGRESSIVE'), 5000.00,    20000.00, 20.00),
      ((SELECT id FROM taxation WHERE name = 'UK_PROGRESSIVE'), 20000.00,   9999999999999999999999999999.99, 40.00);
